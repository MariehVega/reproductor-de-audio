class MultimediaPlayer extends DOMGui {

    constructor(audioTagSelector, tracks, guiParams = undefined) {
        super();

        this.audio = document.querySelector(audioTagSelector);
        this.tracks = tracks;
        this.audio.src = this.tracks[0];
        this.currentTrack = 0;

        this._DOMElements = {
            play: undefined,
            next: undefined,
            prev: undefined,
            title: undefined,
            artist: undefined,
            album: undefined,
            cover: undefined,
            currentTime: undefined,
            totalTime: undefined,
            progressBar: undefined,
            playlistMenu: undefined,
        }

        this.setDOMElements(guiParams);
        this.addListeners();
        this.setPlayerInfo();
    }

    addListeners() {

        this.startTimeUpdateListener();
        this.addButtonListener('play',
            () => {
                if (this.audio.paused) {
                    this.audio.play();
                    this._DOMElements.play.classList.toggle('fa-play', false);
                    this._DOMElements.play.classList.toggle('fa-pause', true);
                } else {
                    this.audio.pause();
                    this._DOMElements.play.classList.toggle('fa-play', true);
                    this._DOMElements.play.classList.toggle('fa-pause', false);
                }

            });

        this.addButtonListener('next',
            () => {
                this.changePlayingSong(this.currentTrack + 1);
            });

        this.addButtonListener('prev',
            () => {
                this.changePlayingSong(this.currentTrack - 1);
            });

        this.addButtonListener('progressBar',
            (e) => {
                let position = e.offsetX;
                let totalW = e.target.clientWidth;
                let progress = position / totalW;
                this.updateProgressBar(progress);
                this.audio.currentTime = this.audio.duration * progress;
            });
    }

    addButtonListener(btnName, callback) {
        this._DOMElements[btnName].onclick = callback;
    }

    changePlayingSong(index) {
        if (index <= this.tracks.length - 1 || index >= 0) {
            this.currentTrack = index;
        }
        if (index > this.tracks.length - 1) {
            this.currentTrack = 0;
        }
        if (index < 0) {
            this.currentTrack = this.tracks.length - 1;
        }
        if (!this.audio.paused) {
            this.audio.src = this.tracks[this.currentTrack];
            this.audio.play();
        } else {
            this.audio.src = this.tracks[this.currentTrack];
        }
        let playing = this._DOMElements.playlistMenu.querySelector('.playing');
        playing.classList.remove('playing');
        let element = this._DOMElements.playlistMenu.children[this.currentTrack];
        element.classList.add('playing');
        this.setPlayerInfo();
    }

    setPlayerInfo() {
        let element = this._DOMElements.playlistMenu.children[this.currentTrack];
        this._DOMElements.title.innerHTML = element.querySelector('.title').innerHTML;
        this._DOMElements.artist.innerHTML = element.querySelector('.artist').innerHTML;
        this._DOMElements.totalTime.innerHTML = element.querySelector('.time').innerHTML;
        this._DOMElements.cover.src = element.querySelector('.cover').src;
    }

    startTimeUpdateListener() {
        this.audio.ontimeupdate = () => {
            let total = this.audio.duration;
            let current = this.audio.currentTime;
            let min = Math.floor(Math.round(current) / 60);
            let sec = Math.round(current) - min * 60;
            let time = `${min}:${sec}`;
            let progress = current / total;
            this._DOMElements.currentTime.innerHTML = time;
            this.updateProgressBar(progress);
            /*
            ** Para que cuándo termine cambie el icono y se detenga la música
            if (this.audio.ended) {
                this._DOMElements.play.classList.toggle('fa-play', true);
                this._DOMElements.play.classList.toggle('fa-pause', false);
            }
            */
            // Para que cambie directamente a la siguiente canción y se reproduzca
            if (this.audio.ended) {
                this.changePlayingSong(this.currentTrack + 1);
                this.audio.play();
            }
        }

    }

    updateProgressBar(progress) {
        this._DOMElements.progressBar.querySelector('.fill').style.transform = `scaleX(${progress})`;
    }
}